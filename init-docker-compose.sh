#!/bin/bash
# Copies basic files to get someone up running fast!

copy_if_not_exists() {
    # Extract the function arguments into variables
    local target_file="$1"
    local source_file="$2"

    # Check if the target file does not exist
    if [[ ! -f $target_file ]]; then
        # Check if the source file exists before trying to copy it
        if [[ -f $source_file ]]; then
            # Copy the source file to the target file path
            cp "$source_file" "$target_file"
            echo "File $target_file copied from $source_file."
        else
            # Handle case where the source file doesn't exist
            echo "Error: $source_file does not exist, cannot copy."
            return 1
        fi
    else
        echo "File $target_file already exists."
    fi
}


copy_if_not_exists "docker-compose.override.yml" "docker-compose.override.yml.sample"
copy_if_not_exists ".var/conf/odoo/odoo.conf" ".var/conf/odoo/odoo.conf.sample"
copy_if_not_exists ".var/conf/nginx/conf.d/default.conf" ".var/conf/nginx/conf.d/default.conf.sample"

